<?php include 'inc/header.php'; ?>
<h2>Ajax - Auto Complete textbox</h2>
<div class="content">
    <style>
        .skill{background: #fba991;width: 234px;padding: 8px;margin-left: 50px;}
        .skill ul{margin: 0;padding: 0;list-style: none;}
        .skill ul li{cursor: pointer;}
    </style>
    <form action="" method="post">
        <table>
            <tr>
                <td>Skill</td>
                <td>:</td>
                <td>
                    <input type="text" name="skill" id="skill" placeholder="Enter user skill name">
                </td>
            </tr>
        </table>
        <div id="skillstatus"></div>
    </form>
</div>
<?php include 'inc/footer.php'; ?>
