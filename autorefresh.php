<?php include 'inc/header.php'; ?>
<h2>Ajax - Auto Refresh Div Content</h2>
<div class="content">
    <style>
        .data{background: #fba991;width: 270px;padding: 8px;margin-left: 82px;}
        .data ul{margin: 0;padding: 0;list-style: none;}
        .data ul li{cursor: pointer;}
    </style>
    <form action="" method="post">
        <table>
            <tr>
                <td>Content</td>
                <td>:</td>
                <td>
                    <textarea name="body" id="body"></textarea>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <input type="submit" name="autosubmit" id="autosubmit" value="Post">
                </td>
            </tr>
        </table>
        <div id="autorefresh"></div>
    </form>
</div>
<?php include 'inc/footer.php'; ?>
