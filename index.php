<?php include 'inc/header.php'; ?>
<h2>Topics: Project List</h2>
<div class="content">
    <div class="topics">
        <ul>
            <li><a href="user.php" target="_blank">01. Ajax Check Username</a></li>
            <li><a href="textbox.php" target="_blank">02. Ajax Auto Complete textbox</a></li>
            <li><a href="passwordButton.php" target="_blank">03. Create A show password button</a></li>
            <li><a href="autorefresh.php" target="_blank">04. Create Auto Refresh Div Content</a></li>
            <li><a href="livesearch.php" target="_blank">05. Ajax Live Data search</a></li>
            <li><a href="autosavedata.php" target="_blank">05. Ajax Auto Save Data</a></li>
        </ul>
    </div>
</div>
<?php include 'inc/footer.php'; ?>