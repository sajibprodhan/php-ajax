<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath . '/../lib/Database.php');

class Project {

    private $db;

    public function __construct() {
        $this->db = new Database();
    }
    public function checkUserName($userName){
        if(empty($userName)){
            echo "<span class='error'>Please inter username</span>";
            exit();
        }
        $query = "SELECT * FROM tbl_user WHERE username = '$userName'";
        $getUser = $this->db->select($query);
        if($getUser){
            echo "<span class='error'>User name not available !</span>";
            exit();
        }else{
            echo "<span class='success'>User name available !</span>";
            exit();
        }
    }
    public function checkSkill($allskill){
        $query = "SELECT * FROM tbl_skill WHERE skill LIKE '%$allskill%'";
        $getSkill = $this->db->select($query);
        $result = '';
        $result .= '<div class="skill"><ul>';
        if($getSkill){
            while($data = $getSkill->fetch_assoc()){
                $result .= '<li>'.$data['skill'].'</li>';
            }
        }else{
            $result .= '<li>No result found !</li>';
        }
        $result .= '</ul></div>';
        echo $result;
    }
    public function autoRefreshData($body){
        $query = "INSERT INTO tbl_body(body) VALUES('$body')";
        $datakh = $this->db->insert($query);
    }
    public function getWithoutRefresh(){
        $query = "SELECT * FROM tbl_body ORDER BY id DESC";
        $getdata = $this->db->select($query);
        $result = '';
        $result .= '<div class="data"><ul>';
        if($getdata){
            while($data = $getdata->fetch_assoc()){
                $result .= '<li>'.$data['body'].'</li>';
            }
        }else{
            $result .= '<li>No result found.</li>';
        }
        $result .= '</ul></div>';
        echo $result;
        
    }
    public function liveSearch($search){
        $query = "SELECT * FROM tbl_search WHERE username LIKE '%$search%'";
        $getdata = $this->db->select($query);
        if($getdata){
            $data = '';
            $data .= '<table class="tblone"><tr>'
                    . '<th>UserName</th>'
                    . '<th>Name</th>'
                    . '<th>Email</th>'
                    . '</tr>';
            while($row = $getdata->fetch_assoc()){
                $data .= '<tr>'
                        . '<td>'.$row["username"].'</td>'
                        . '<td>'.$row["name"].'</td>'
                        . '<td>'.$row["email"].'</td>';
            }
            echo $data;
        }else{
            echo "Data not found";
        }
    }
    public function contentSave($content,$contentid){
        if($contentid != ''){
            $query = "UPDATE tbl_save SET content = '$content' WHERE contentid = '$contentid'";
            $update_now = $this->db->update($query);
        }else{
            $query = "INSERT INTO tbl_save(content, status) VALUES('$content', 'draft')";
            $insert_data = $this->db->insert($query);
            $lastid = $this->db->link->insert_id;
            echo $lastid;
            exit();
        }
    }

}

?>