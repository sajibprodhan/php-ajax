$(document).ready(function () {
    // Ajax Check Username --
    $('#username').blur(function () {
        var username = $(this).val();
        $.ajax({
            url: "check/checkuser.php",
            method: "POST",
            data: {username: username},
            dataType: "text",
            success: function (data) {
                $('#userstatus').html(data);
            }
        });
    });

    // Ajax Auto Complete textbox --
    $("#skill").keyup(function() {
        var skill = $(this).val();
        if (skill != '') {
            $.ajax({
                url: "check/checkskill.php",
                method: "POST",
                data: {skill:skill},
                success: function (data) {
                    $('#skillstatus').fadeIn();
                    $('#skillstatus').html(data);
                }
            });
        }
    });
    $(document).on('click', 'li', function (){
        $("#skill").val($(this).text());
        $('#skillstatus').fadeOut();
    });
    
    // Show Password Button
    $("#showpassowrd").on('click', function(){
        var pass = $("#password");
        var fieldtype = pass.attr('type');
        if(fieldtype == 'password'){
            pass.attr('type', 'text');
            $(this).text("Hide Password");
        }else{
            pass.attr('type', 'text');
            $(this).text("Show Password");
        }
    });
    
    //Auto Refresh Div Conten
    $("#autosubmit").click(function(){
        var content = $("#body").val();
        if($.trim(content) != ''){
            $.ajax({
                url:"check/checkrefresh.php",
                method:"POST",
                data:{body:content},
                datatype:"text",
                success:function (data){
                    $("#body").val("");
                }
            });
            return false;
        }
    });
    setInterval(function(){
        $("#autorefresh").load("check/getrefresh.php").fadeIn("slow");
    }, 1000);
    
    //Ajax Live Data search
    $("#livesearch").keyup(function(){
        var live = $(this).val();
        if(live != ''){
            $.ajax({
                url:"check/livesearh.php",
                method:"POST",
                data:{search:live},
                datatype:"text",
                success:function(data){
                    $('#statuslive').html(data);
                }
            });
        }else{
            $('#statuslive').html("");
        }
    });
////Auto Save Data
    function autosave(){
        var content = $("#content").val();
        var contentid = $("#contentid").val();
        if(content != ''){
            $.ajax({
                url:"check/autosave.php",
                method:"POST",
                data:{content:content,contentid:contentid},
                datatype:"text",
                seccess:function(data){
                    if(data != ''){
                        $("#contentid").val(data);
                    }
                    $("#statussave").text("Content save as draft..");
                    setInterval(function(){
                        $("#statussave").text("");
                    }, 2000);
                }
            });
        }
    }
    setInterval(function(){
        autosave();
    }, 10000);


});  